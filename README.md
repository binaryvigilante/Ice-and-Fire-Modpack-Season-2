# Ice and Fire Modpack Season 2
 My modpack for Minecraft 1.16.5 for my YouTube series Ice and Fire Season 2!

## Installation Guide

1. Download the modpack from my website (https://binaryvigilante.com/downloads/ice-and-fire-season-2-modpack/)
2. Install Forge for Minecraft 1.16.5
3. Extract the `mods` folder and the `configs` folder to your `.minecraft` folder
4. You are good to go :)
